# ADB Wireless

ADB Wireless is an open source app for wireless debugging in Android Studio.
### UI inspiration

[ADB Wireless By Henry](https://play.google.com/store/apps/details?id=za.co.henry.hsu.adbwirelessbyhenry&hl=en)

### Install instructions
  - Clone and build the project.
  - It will complain for a missing google-services.json file.
  - Connect to a firebase project.
  - Download and add google-services.json to the app folder.
    (Used Crashlytics in Firebase for the sake of variety of Android devices/versions)
  
### License:

GNU General Public License V3

Pull requests welcome.

Copyright: Utsav Singhal
