package in.blogspot.choosejava.adbwireless;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

public class AboutActivity extends AppCompatActivity {

    private TextView aboutTextView;
    private TextView linkTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        aboutTextView = findViewById(R.id.about_textView);
        String aboutText = Utils.readRawTextFile(this, R.raw.about_text2);
        aboutTextView.setText(Html.fromHtml(aboutText));

        //Set link text
        StringBuilder linkTextTemp = new StringBuilder();
        linkTextTemp.append("<u>");
        linkTextTemp.append("(View listing)");
        linkTextTemp.append("</u>");

        linkTextView = findViewById(R.id.about_link_textView);
        linkTextView.setText(Html.fromHtml(linkTextTemp.toString()));

        linkTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Visit URL
                //https://play.google.com/store/apps/details?id=za.co.henry.hsu.adbwirelessbyhenry

                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=za.co.henry.hsu.adbwirelessbyhenry")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=za.co.henry.hsu.adbwirelessbyhenry")));
                }
            }
        });
    }
}
